from dataclasses import dataclass
from itertools import chain
from typing import List

from roid_rage.model.field import Field
from roid_rage.driver import Driver
from roid_rage.model.roid import Roid
from roid_rage.model.ship import Ship


@dataclass
class Game:
    field: Field
    roids: List[Roid]
    ships: List[Ship]
    driver: Driver

    def update(self, time_delta):
        for entity in chain(self.roids, self.ships):
            entity.update(time_delta)

        # TODO: Wrap or delete objects that have moved outside the field.

        for ship in self.field.ships:
            self.driver.update(ship, self.field, time_delta)
 