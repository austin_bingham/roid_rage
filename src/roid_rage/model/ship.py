from roid_rage.model.entity import Entity


class Ship(Entity):
    def __init__(self, max_rotational_speed, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_rotational_speed = max_rotational_speed

    def _set_rotational_speed(self, s):
        s = min(self.max_rotational_speed, s)
        super()._set_rotational_speed(s)