from roid_rage.geom import Vector


class Entity:
    def __init__(self, position, radius, velocity, orientation, rotational_speed=0):
        self.position = position
        self.radius = radius
        self.velocity = velocity
        self.orientation = orientation
        self._rotational_speed = rotational_speed

    @property
    def rotational_speed(self):
        return self._rotational_speed

    @rotational_speed.setter
    def rotational_speed(self, s):
        self._set_rotational_speed(s)

    def _set_rotational_speed(self, s):
        self._rotational_speed = s

    def update(self, time_delta):
        motion_vec = Vector(magnitude=self.velocity.magnitude * time_delta,
                            bearing=self.velocity.bearing)
        self.position = self.position + motion_vec

        self.orientation += self.rotational_speed * time_delta
