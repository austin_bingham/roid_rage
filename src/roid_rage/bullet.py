from roid_rage.sprites import RenderedSprite
from roid_rage.util import find_velocity


class Bullet(RenderedSprite):
    def __init__(self,
                 speed,
                 radius,
                 direction,
                 *args, **kwargs):
        self.radius = radius

        super().__init__(width=radius * 2,
                         height=radius * 2,
                         *args, **kwargs)

        self.collision_radius = radius
        self.velocity = find_velocity(speed, direction)

    def render(self, draw):
        draw.ellipse([0, 0, self.radius * 2, self.radius * 2],
                     fill=(100, 100, 100, 255))
