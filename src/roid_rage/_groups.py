import pygame

roids = pygame.sprite.Group()
ships = pygame.sprite.Group()
bullets = pygame.sprite.Group()
splosions = pygame.sprite.Group()

class GroupContainer:
    def __init__(self, groups):
        self.groups = groups

    def add(self, g):
        self.groups.append(g)

    def remove(self, g):
        self.groups.remove(g)

    def __contains__(self, g):
        return g in self.groups

    def __iter__(self):
        return iter(self.groups)

    def draw(self, *args, **kwargs):
        for g in self.groups:
            g.draw(*args, **kwargs)

    def update(self, *args, **kwargs):
        for g in self.groups:
            g.update(*args, **kwargs)

all_objects = GroupContainer([
    roids,
    ships,
    bullets,
    splosions,
    ])
