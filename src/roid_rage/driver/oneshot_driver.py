from roid_rage.collision import calculate_collision_vector, will_collide
from roid_rage import geom
from roid_rage.bullet import Bullet

from .driver import Driver
# from roid_rage.geom import Point


class OneShotDriver(Driver):
    """A driver which only allows one shot per roid at a time.

    It keeps track of roids for which there is a shot in flight. For
    these roids, it won't shoot again.
    """

    def __init__(self):
        self.target = None
        self.collisions = []
        self.shots = {}

    def _find_target(self, ship, model, roids):
        for roid in roids:
            # Figure out lead position for hitting target
            coll_pos, target_vec = calculate_collision_vector(
                ship.point,
                ship.gun.bullet_speed,
                roid.point,
                roid.speed,
                roid.direction)

            if coll_pos is None:
                continue

            if coll_pos not in model.field:
                continue

            return (roid, target_vec)

        return (None, None)

    def update(self, ship, game, time_delta):
        ship.orientation += 1
        return

        # Only keep shot entries for live bullets
        # self.shots = dict(
        #    ((k, v) for k, v in self.shots.items() if v in game.bullet_list))

        # if not model.roid_list:
        #     return

        # See if the current target is gone
        if self.target is not None:
            if (self.target in game.roids
                    and self.target in self.shots):
                return
            else:
                self.target = None

        # Find all roids that will hit the ship
        self.collisions = [
            r for r in game.roids
            if will_collide(ship, r)]

        # filter out roids for which a shot already exists.
        roids = [
            r for r in self.collisions
            if r not in self.shots
        ]

        # If we've already shot at all colliders, consider all roids.
        if not roids:
            roids = [
                r for r in model.roid_list
                if r not in self.shots
            ]

        if not roids:
            return

        # Sort potential targets by nearest angular distance
        roids = sorted(
            roids,
            key=lambda r: geom.angular_distance(ship.orientation, (ship.point - r.point).bearing))

        self.target, target_vec = self._find_target(ship, model, roids)

        if self.target is not None:
            delta = geom.angular_distance(
                ship.orientation,
                target_vec.bearing)

            # Change orientation as far in target direction as possible.
            ship.rotational_speed = delta / time_delta

            self.fire(ship, self.target, model)

    def fire(self, ship, target, model):
        if not ship.gun.can_fire:
            return

        bullet = Bullet(
            speed=ship.gun.bullet_speed,
            radius=ship.gun.bullet_size,
            direction=ship.orientation,
            center_x=ship.center_x,
            center_y=ship.center_y)

        if not will_collide(bullet, target):
            return

        self.shots[self.target] = bullet
        self.target = None
        model.bullet_list.append(bullet)
