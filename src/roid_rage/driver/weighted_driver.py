import math

import pygame

from .. import collision, geom, groups


def angular_distance_classifier(ship, target, coll_pos, target_vec):
    return abs(geom.angular_distance(
        ship.orientation,
        target_vec.bearing) / math.pi)

def distance_classifier(ship, target, coll_pos, target_vec):
    vec = target.position - ship.position
    board_size = math.sqrt(math.pow(ship.board.width, 2) + math.pow(ship.board.height, 2))
    return board_size - abs(vec.distance)

class WeightedDriver:
    """A driver which combines differente target classifiers.
    """
    def __init__(self, ship):
        self.ship = ship
        self.target = None
        self.collisions = []
        self.shots = {}
        self.classifiers = [
            angular_distance_classifier,
            distance_classifier,
        ]

    def hittable(self, roid):
        coll_pos, target_vec = collision.calculate_collision_vector(
                self.ship.position,
                self.ship.gun.bullet_speed,
                roid.position,
                roid.speed,
                roid.direction)

        if coll_pos is None:
            return False

        if not self.ship.board.contains(coll_pos):
            return False

        return True

    def find_target(self, roids):
        for roid in roids:
            # Figure out lead position for hitting target
            coll_pos, target_vec = collision.calculate_collision_vector(
                self.ship.position,
                self.ship.gun.bullet_speed,
                roid.position,
                roid.speed,
                roid.direction)

            if coll_pos is None:
                continue

            if not self.ship.board.contains(coll_pos):
                continue

            return (roid, target_vec)

        return (None, None)

    def update(self, time_delta):
        # Only keep shot entries for live bullets
        self.shots = dict(
            ((k,v) for k,v in self.shots.items() if v in groups.bullets))

        if not groups.roids:
            return

        if self.target is not None:
            if (self.target in groups.roids
                and self.target in self.shots):
                return
            else:
                self.target = None

        # Find all roids that will hit the ship
        self.collisions = [r for r in groups.roids if collision.will_collide(self.ship.rect, self.ship.speed, self.ship.orientation, r.rect, r.speed, r.direction)]

        # filter out roids for which a shot already exists.
        roids = list(filter(
            lambda r: r not in self.shots,
            self.collisions))

        # If we've already shot at all colliders, consider all roids.
        if not roids:
            roids = list(filter(
                lambda r: r not in self.shots,
                groups.roids))

        # calculate coll_pos and target_vec for each roid
        def target_roid(roid):
            coll_pos, target_vec = collision.calculate_collision_vector(
                self.ship.position,
                self.ship.gun.bullet_speed,
                roid.position,
                roid.speed,
                roid.direction)
            return (roid, coll_pos, target_vec)
        roids = [target_roid(r) for r in roids]

        # Remove roids which can't be hit due to off-screen collisions
        roids = filter(lambda x: x[1] is not None, roids)

        if not roids:
            return

        # Let each of the classifiers chime in...
        targets = {}
        for roid in roids:
            targets[roid] = 0
            for classifier in self.classifiers:
                targets[roid] = targets[roid] + classifier(self.ship, *roid)
        targets = sorted(
            ((r[0], r[1], r[2], w) for r, w in targets.items()),
            key=lambda x: x[3], reverse=True)

        if not targets:
            return

        self.target = targets[0][0]

        delta = geom.angular_distance(
            self.ship.orientation,
            targets[0][2].bearing)

        # Change orientation as far in target direction as possible.
        self.ship.rotational_speed = delta / time_delta

        self.fire()

    def fire(self):
        if not self.ship.gun.can_fire:
            return

        bullet_rect = pygame.rect.Rect(
            self.ship.position.x,
            self.ship.position.y,
            self.ship.gun.bullet_size,
            self.ship.gun.bullet_size)

        if not collision.will_collide(
                bullet_rect,
                self.ship.gun.bullet_speed,
                self.ship.orientation,
                pygame.Rect(
                    self.target.position.x,
                    self.target.position.y,
                    1, 1),
                self.target.speed,
                self.target.direction):
            return

        b = self.ship.gun.fire(
            self.ship.position,
            self.ship.orientation,
            self.ship.board)

        assert b is not None, 'We should not have tried to fire!'

        self.shots[self.target] = b
        self.target = None
