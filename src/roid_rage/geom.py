import math


class Point:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __getitem__(self, idx):
        if idx == 0:
            return self.x
        if idx == 1:
            return self.y
        else:
            raise IndexError()

    def __repr__(self):
        return 'Point(x={0}, y={1}'.format(self.x, self.y)

    def __sub__(self, src):
        '''Subtract one point from another to calculate the vector between.

        This find the vector from ``src`` to this point.

        :param x: The starting point
        :return: The vector from ``src`` to this.
        '''

        return Vector(
            magnitude=math.sqrt(pow(self.x - src.x, 2) +
                                pow(self.y - src.y, 2)),
            bearing=math.atan2(self.y - src.y, self.x - src.x))

    def __add__(self, vec):
        return Point(
            self.x + math.cos(vec.bearing) * vec.magnitude,
            self.y + math.sin(vec.bearing) * vec.magnitude)


class Vector:
    def __init__(self, magnitude, bearing):
        self.magnitude = magnitude
        self.bearing = bearing


def normalize(b):
    '''Normalize a bearing to be between 0 and 2*pi.

    :param b: The bearing to normalize.
    '''

    while b < 0:
        b += math.pi * 2.0

    while b > math.pi * 2.0:
        b -= math.pi * 2.0

    return b


def angular_distance(f, t):
    '''Shortest angular distance from `f` to `t`.

    :param f: Starting bearing.
    :param t: Ending bearing.
    '''

    f = normalize(f)
    t = normalize(t)

    dist = t - f

    if abs(dist) > math.pi:
        dist = 2.0 * math.pi - abs(dist)
        dist *= -1

    return dist


def bearing_between(brg, start_brg, end_brg):
    '''Calculate if a bearing is between two other bearings.

    A bearing is between two other if, by starting at the
    start_bearing and sweeping to the end_bearing, the target bearing
    is passed.

    :param brg: The target bearing.
    :param start_brg: The start bearing.
    :param end_brg: The end bearing.
    '''

    # Does the sweep straddle 0?
    if start_brg > end_brg:
        return not (brg < start_brg and brg > end_brg)
    else:
        return brg >= start_brg and brg >= end_brg


# def scale_rect(rect, factor):
#     x_delta = rect.width - (factor * rect.width)
#     y_delta = rect.height - (factor * rect.height)
#     return pygame.Rect(
#         rect.left + x_delta / 2.0,
#         rect.top + y_delta / 2.0,
#         rect.width * factor,
#         rect.height * factor)

def velocity_to_speed(v):
    return math.sqrt(math.pow(v[0], 2) + math.pow(v[1], 2))


def velocity_to_direction(v):
    return math.atan2(v[1], v[0])