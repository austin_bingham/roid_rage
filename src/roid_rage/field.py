import dataclasses


@dataclasses.dataclass
class Field:
    width: int
    height: int
    fringe: int

    @property
    def left_limit(self):
        return 0 - self.fringe

    @property
    def right_limit(self):
        return self.width + self.fringe

    @property
    def bottom_limit(self):
        return 0 - self.fringe

    @property
    def top_limit(self):
        return self.height + self.fringe

    def __contains__(self, point):
        x, y = point[0], point[1]
        return all((
            x >= self.left_limit,
            x <= self.right_limit,
            y >= self.bottom_limit,
            y <= self.top_limit
        ))