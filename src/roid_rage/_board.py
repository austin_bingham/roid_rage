'The basic definition of the game board.'


class Board:
    'Defines the extents of the board.'

    def __init__(self, dims):
        self.dims = dims

    @property
    def width(self):
        'The width in pixels of the board.'
        return self.dims[0]

    @property
    def height(self):
        'The height in pixels of the board.'
        return self.dims[1]

    def contains(self, pos):
        return (pos.x > 0
                and pos.y > 0
                and pos.x <= self.dims[0]
                and pos.y <= self.dims[1])
