import math

from roid_rage.sprites import RenderedSprite
from roid_rage import util


class Fragment(RenderedSprite):
    def __init__(self,
                 max_age,
                 speed,
                 direction,
                 *args, **kwargs):
        super().__init__(width=4, height=4, *args, **kwargs)
        self.max_age = max_age
        self._age = 0
        self.velocity = util.find_velocity(speed, direction)

        self.original_x = self.center_x
        self.original_y = self.center_y

    def render(self, draw):
        draw.ellipse([0, 0, self.width, self.height], 
                     fill=(100, 100, 100, 255), 
                     outline=(100, 100, 100, 255))

    def update(self, time_delta):
        super().update(time_delta)
        self._age += time_delta
        if self._age > self.max_age:
            self.kill()


def explosion(center_x, center_y):
    """Generate a sequence of sprites comprising an explosion.
    """
    for _ in range(10):
        yield Fragment(
            max_age=util.rand(1, 2),
            speed=util.rand(1, 2),
            direction=util.rand(0, math.pi * 2),
            center_x=center_x,
            center_y=center_y,
        )
