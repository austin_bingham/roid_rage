import array, math
import cairo, numpy, pygame

mem_fudge = 1.2

class Drawable(pygame.sprite.Sprite):
    '''A base class for things that draw to a cairo context.
    '''
    def __init__(self,
                 size,
                 position,
                 orientation):
        super(Drawable, self).__init__()

        self.size = size
        self.position = position
        self.orientation = orientation
        self.color = (1, 1, 1)

        self.data = None
        self._image = None

    def invalidate(self):
        self._image = None

    def render_image(self, ctxt):
        '''Draw the drawable to a cairo Context.

        The context has been scaled to (1.0, 1.0) and positioned at
        (0,0).

        :param ctxt: A `cairo.Context` object.
        '''
        raise NotImplementedError()

    def _get_image(self):
        # We make the data array larger than the basic image rect size
        # because rotation will make the drawing spill over the edge.

        # But how much bigger? An easy answer is two times, but that's
        # also wasteful. The math-ish answer is sqrt(2) times bigger,
        # two which we add 10% to avoid line-thickness issues, etc.

        if not self._image:
            mem_size = int(math.sqrt(2.0) * mem_fudge * self.size)
            self.data = numpy.zeros(mem_size * mem_size, dtype=numpy.int32)

            # This is our memory expansion
            mem_factor = math.sqrt(2.0) * mem_fudge
            size = int(mem_factor * self.size)
            surf = cairo.ImageSurface.create_for_data(self.data, cairo.FORMAT_ARGB32, size, size, size * 4)
            ctxt = cairo.Context(surf)

            ctxt.translate(size / 2.0, size / 2.0)
            ctxt.rotate(self.orientation)
            ctxt.scale(self.size, self.size)
            ctxt.translate(-0.5, -0.5)
            ctxt.move_to(0,0)

            ctxt.set_line_width(4 * mem_factor / size)
            ctxt.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
            ctxt.set_source_rgba(*self.color)

            self.render_image(ctxt)

            self._image = pygame.image.frombuffer(self.data.tostring(), (size, size), 'ARGB')

        return self._image

    image = property(
        _get_image,
        doc="The ship's image.")

    def _get_rect(self):
        r = self.image.get_rect()
        return r.move(self.position.x - r.width / 2,
                      self.position.y - r.height / 2)

    rect = property(
        _get_rect,
        doc="The ship's rect for sprite purposes.")
