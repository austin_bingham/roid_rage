import pygame
import pygame.event

def post_event(event_type, *args):
    pygame.event.post(
        pygame.event.Event(
            event_type, *args))

HIGHLIGHT_ROIDS = pygame.USEREVENT + 1
RESPAWN_ROIDS = pygame.USEREVENT + 2
