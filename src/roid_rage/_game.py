'The top-level game executive.'

import math
import random

import pygame
from pygame.locals import (KEYDOWN,
                           K_c,
                           K_ESCAPE,
                           K_r,
                           MOUSEBUTTONDOWN,
                           MOUSEBUTTONUP,
                           QUIT)

# from roid_rage import (driver,
#                        event,
#                        geom,
#                        groups,
#                        gun,
#                        roid,
#                        ship,
#                        splosion,
#                        widget)

from roid_rage.board import Board

if not pygame.font:
    print('Warning, fonts disabled')

if not pygame.mixer:
    print('Warning, sound disabled')

# SPLOSION_MAP = {
#     'frag': splosion.Fragments,
#     'text': splosion.Text,
# }


# @baker.command(
#     default=True,
#     params={
#         'size_x': 'The width of the board. [>0]. Default=1500.',
#         'size_y': 'The height of the board. [>0]. Default=900.',
#         'count': 'The number of raging roids. [>=0]. Default=10.',
#         'min_size': 'Minimum roid size. [>0]. Default=40.',
#         'max_size': 'Maximum roid size. [>=min_size]. Default=80.',
#         'min_speed': 'Minimum roid speed. [>0]. Default=0.05.',
#         'max_speed': 'Maximum roid speed. [>=max_speed]. Default=0.1.',
#         'bullet_speed': 'Speed of bullets. [>0]. Default=0.2.',
#         'bullet_size': 'Size of bullets. [>0]. Default=2.',
#         'fire_rate': 'The time in milliseconds between bullets [>0]. '
#                      'Default=200.',
#         'bullet_count': 'The maximum number of active bullets [>=0]. '
#                         'Default=5.',
#         'rotational_speed': 'Rotational speed of ship [>=0]. Default=0.005.',
#         'splosion_style': 'The style of explosion [text, frag]. '
#                           'Default=frag.'})
def run(size_x=1500,
        size_y=900,
        count=10,
        min_size=40,
        max_size=80,
        min_speed=0.05,
        max_speed=0.1,
        bullet_speed=0.2,
        bullet_size=2,
        fire_rate=200,
        bullet_count=5,
        rotational_speed=0.005,
        splosion_style='frag'):

    if size_x < 0 or size_y < 0:
        raise ValueError('Dimensions must be greater than 0.')
    if count < 0:
        raise ValueError('Number of roids must be greater than 0.')
    if min_size < 0:
        raise ValueError('Min. size must be greater than 0.')
    if max_size < min_size:
        raise ValueError('Max. size must not be less than min. size.')
    if min_speed < 0:
        raise ValueError('Min. speed must be greater than 0.')
    if max_speed < min_speed:
        raise ValueError('Max. speed must not be less than min. speed.')
    if bullet_speed <= 0:
        raise ValueError('Bullet speed must be positive.')
    if fire_rate <= 0:
        raise ValueError('Fire rate must be positive.')
    if bullet_count < 0:
        raise ValueError('Bullet count must not be negative.')

    board = Board((size_x, size_y))
    background_color = (0, 0, 0)
    # framerate = 60

    def new_size(min_size, max_size):
        return int(random.random() * (max_size - min_size) + min_size)

    def new_speed(min_speed, max_speed):
        return random.random() * (max_speed - min_speed) + min_speed

    # Initialize Everything
    pygame.init()
    screen = pygame.display.set_mode(board.dims)

    pygame.display.set_caption('`roid Rage')
    pygame.mouse.set_visible(1)

    # drivers = [
    #     driver.WeightedDriver(
    #         ship.make_ship(
    #             ship.Ship,
    #             gun.Gun(bullet_size=bullet_size,
    #                     bullet_speed=bullet_speed,
    #                     fire_rate=fire_rate,
    #                     bullet_count=bullet_count),
    #             size=20,
    #             position=geom.Point(
    #                 board.width / 2,
    #                 board.height / 2),
    #             direction=0,
    #             rotational_speed=rotational_speed,
    #             board=board))
    # ]

    # Create The Backgound
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill(background_color)

    # Display The Background
    pygame.display.flip()

    # Prepare Game Objects
    # clock = pygame.time.Clock()

    # def respawn_roids():
    #     for _ in range(count):
    #         roid.make_roid(
    #             roid.Roid,
    #             size=new_size(min_size, max_size),
    #             speed=new_speed(min_speed, max_speed),
    #             direction=random.random() * 2 * math.pi,
    #             position=geom.Point(
    #                 random.random() * board.width,
    #                 random.random() * board.height),
    #             orientation=random.random() * 7,
    #             rotation_speed=random.random() * 0.05 - 0.025,
    #             board=board)

    # widget.setup()
    # widget.show()

    # highlight_targets = False

    # Main Loop
    going = True
    while going:
        # if not groups.roids:
        #     respawn_roids()

        # time_delta = clock.tick(framerate)

        # Handle Input Events
        for evt in pygame.event.get():
            if evt.type == QUIT:
                going = False
            # elif evt.type == event.HIGHLIGHT_ROIDS:
            #     highlight_targets = evt.checked
            # elif evt.type == event.RESPAWN_ROIDS:
            #     respawn_roids()
            # elif evt.type == KEYDOWN and evt.key == K_ESCAPE:
            #     going = False
            # elif evt.type == KEYDOWN and evt.key == K_r:
            #     event.post_event(event.RESPAWN_ROIDS)
            # elif evt.type == KEYDOWN and evt.key == K_c:
            #     widget.toggle()
            # elif evt.type == MOUSEBUTTONDOWN:
            #     pass
            # elif evt.type == MOUSEBUTTONUP:
            #     pass

        # allsprites.update()
        # background.fill(background_color)

        # update all objects.
        # for d in drivers:
        #     d.update(time_delta)

        # for r in groups.roids:
        #     for d in drivers:
        #         r.color = (1, 1, 1)
        #         if highlight_targets:
        #             if r in d.shots:
        #                 r.color = (1, 0, 1)
        #             elif r == d.target:
        #                 r.color = (1, 0, 0)
        #             elif r in d.collisions:
        #                 r.color = (1, 1, 0)

        # groups.all_objects.update(time_delta)

        # # Splode the roids
        # hits = pygame.sprite.groupcollide(
        #     groups.roids,
        #     groups.bullets,
        #     True, True,
        #     collided=pygame.sprite.collide_circle)

        # for r in hits:
        #     if r.size > 20:
        #         for _ in range(2):
        #             roid.make_roid(
        #                 roid.Roid,
        #                 size=r.size / 2,
        #                 speed=r.speed * 1.5,
        #                 direction=random.random() * 2 * math.pi,
        #                 position=r.position,
        #                 orientation=random.random() * 7,
        #                 rotation_speed=r.rotation_speed * 1.5,
        #                 board=board)
        #     splosion.make_splosion(
        #         SPLOSION_MAP[splosion_style],
        #         2 * r.size,
        #         r.position,
        #         1000)

        # # Kill the ship
        # hits = pygame.sprite.groupcollide(groups.ships,
        #                                   groups.roids,
        #                                   False,
        #                                   False)
        # for s, r in hits.items():
        #     if r:
        #         s.position = geom.Point(
        #             int(board.width / 4 + random.random() * board.width / 2),
        #             int(board.height / 4 + random.random() * board.height / 2))

        # groups.all_objects.draw(background)

        # Draw Everything
        # allsprites.draw(screen)
        pygame.display.flip()

    pygame.quit()


def main():
    run()


if __name__ == '__main__':
    main()
