# Functions for determining how to hit a moving object.

import logging
import math

from roid_rage import geom

log = logging.getLogger(__name__)


def solve_quadratic(a, b, c):
    '''Find real roots for a quadratic of the form:

      ax^2 + bx + c

    :param a: The "a" in the quadratic
    :param b: The "b" in the quadratic
    :param c: The "c" in the quadratic
    :return: A list of real roots, sized 0, 1, or 2
    '''

    disc = math.pow(b, 2) - 4 * a * c

    if disc < 0:
        roots = []

    elif disc == 0:
        roots = [(-1 * b) / (2 * a)]

    else:
        sqrt_disc = math.sqrt(disc)
        roots = [
            (-1 * b + sqrt_disc) / (2 * a),
            (-1 * b - sqrt_disc) / (2 * a)
        ]

    log.info('solve_quadratic(a={}, b={}, c={}) = {}'.format(
        a, b, c, roots))
    return roots


def will_collide(sprite1, sprite2):
    '''Determine if two sprites will collide.
    '''

    # Calcuate object 1's speed relative to object 2.
    x_delta_spd = (math.cos(sprite1.direction) * sprite1.speed - math.cos(sprite2.direction) * sprite2.speed)

    # Find the time-delta range during which these objects collide in
    # x.
    x_coll_range = []
    if x_delta_spd != 0:
        x_coll_range = sorted(
            [(sprite2.left - sprite1.right) / x_delta_spd,
            (sprite2.right - sprite1.left) / x_delta_spd])


    # If the time range is purely in the past, there is no collision
    if all((x <= 0 for x in x_coll_range)):
        return False

    # Do the same for y...
    y_delta_spd = (math.sin(sprite1.direction) * sprite1.speed - math.sin(sprite2.direction) * sprite2.speed)

    # Find the time-delta range during which these objects collide in
    # y.
    y_coll_range = []
    if y_delta_spd != 0:
        y_coll_range = sorted(
            [(sprite2.top - sprite1.bottom) / y_delta_spd,
            (sprite2.bottom - sprite1.top) / y_delta_spd])

    # If the time range is purely in the past, there is no collision
    if all((y <= 0 for y in y_coll_range)):
        return False

    # There's a collision only if there's an overlap in the time
    # windows
    return not (x_coll_range[1] < y_coll_range[0]
                or y_coll_range[1] < x_coll_range[0])


def calculate_collision_point(
        position,
        speed,  # speed of projectile
        target_pos,
        target_speed,  # speed of target
        target_dir):
    '''Calculate point of collision given target position + velocity
    and projectile speed + firing position.

    :return: Point of collision, or None if no collision possible
    '''

    # Solve for delta-t, the time at which the target and the
    # projectile will be equally distant from `position`. This
    # involves solving a quadratic equation, hence the a, b, and c.
    dx = target_pos.x - position.x
    dy = target_pos.y - position.y
    a = pow(speed, 2) - pow(target_speed, 2)
    b = -2 * (target_speed * math.cos(target_dir) * dx +
              target_speed * math.sin(target_dir) * dy)
    c = -1 * (pow(dx, 2) + pow(dy, 2))

    roots = [r for r in solve_quadratic(a, b, c) if r >= 0]
    if not roots:
        return None

    # This is how far in the future the collision will occur
    dt = min(roots) if roots else None

    coll_x = dt * target_speed * math.cos(target_dir) + target_pos.x
    coll_y = dt * target_speed * math.sin(target_dir) + target_pos.y
    return geom.Point(coll_x, coll_y)


def calculate_collision_vector(position,
                               speed,
                               target_pos,
                               target_speed,
                               target_dir):
    '''Calculate vector from `position` to target that will result in
    a collision given the other parameters.

    :param position: The firing position.
    :param speed: The speed of the projectile.
    :param target_pos: Initial position of target to hit.
    :param target_speed: Speed of target to hit.
    :param target_dir: Direction (of movement) of target

    :return: A tuple (collision-position, collision-vector) if one
        exists, or (None, None) if not.
    '''

    coll_pos = calculate_collision_point(
        position=position,
        speed=speed,
        target_pos=target_pos,
        target_speed=target_speed,
        target_dir=target_dir)

    if coll_pos is None:
        return (None, None)

    return (coll_pos, coll_pos - position)
