import math
import random


# trace_log = logging.getLogger(__name__ + '.trace')

# @decorator.decorator
# def trace(f, *args, **kwargs):
#     trace_log.debug('{0}(args={1}, kwargs={2})'.format(f.__name__, args, kwargs))
#     return f(*args, **kwargs)

# def consume(i):
#     '''Iterate over an iterable.'''

#     for _ in i: pass

def sign(x):
    '''Get sign of a number.'''
    return 1 if x >= 0 else -1

# main_dir = os.path.split(os.path.abspath(__file__))[0]
# data_dir = os.path.join(main_dir, 'data')

# #functions to create our resources
# def load_image(name, colorkey=None):
#     fullname = os.path.join(data_dir, name)
#     try:
#         image = pygame.image.load(fullname)
#     except pygame.error as e:
#         print ('Cannot load image:', fullname)
#         raise SystemExit(str(e))
#     image = image.convert_alpha()
#     if colorkey is not None:
#         if colorkey is -1:
#             colorkey = image.get_at((0,0))
#         image.set_colorkey(colorkey, RLEACCEL)
#     return image, image.get_rect()

# def in_order(a, b):
#     '''Put two values in order.

#     :return: A tuple with the smaller of ``a`` and ``b`` first and the
#       larger second.
#     '''
#     if a < b: return (a,b)
#     else: return (b, a)

def rand(start, end):
    '''Find a random real between ``start`` and ``end`` (inclusive).

    :param start: The lowest possible return value.
    :param end: The highest possible return value.

    :return: A real number in the range [``start``, ``end``].
    '''
    return random.random() * (end - start) + start


def find_velocity(speed, direction):
    """Convert a speed and direction (radians) to a (x,y) velocity tuple.

    0-radians is straight up in this formula.
    """
    return (math.sin(direction) * speed, math.cos(direction) * speed)
