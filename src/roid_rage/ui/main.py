import random

import arcade

from roid_rage.driver import OneShotDriver
from roid_rage.field import Field
from roid_rage.gun import Gun
from roid_rage.model import Model
from roid_rage.ship import Ship
from roid_rage.roid import Roid


class RoidRage(arcade.Window):
    def __init__(self, game, ui_model):
        super().__init__(game.field.width, game.field.height, "Roid Rage!")
        self.model = model
        self.ui_model = ui_model

    def start_new_game(self):
        for _ in range(1):
            self.model.roid_list.append(
                Roid(center_x=random.randint(0, self.model.field.width),
                     center_y=random.randint(0, self.model.field.height),
                     radius=random.randint(30, 50),
                     velocity=(random.randint(1, 6) - 3,
                               random.randint(1, 6) - 3)))

    def on_draw(self):
        arcade.start_render()
        self.model.draw()

    def on_key_press(self, symbol, modifiers):
        pass

    def on_key_release(self, symbol, modifiers):
        pass

    def on_update(self, delta_time):
        self.model.update(delta_time)

        # Let the driver do something
        for ship in self.model.ship_list:
            self.driver.update(ship, self.model, delta_time)

def main():
    field = Field(width=800, height=600, fringe=100)
    model = Model(field=field)
    gun = Gun(bullet_size=2, bullet_speed=10, fire_rate=1)
    ship = Ship(center_x=400, center_y=300, width=20, height=15, gun=gun)
    model.ship_list.append(ship)
    driver = OneShotDriver()
    window = RoidRage(model, driver)
    window.start_new_game()
    arcade.run()


if __name__ == "__main__":
    main()
