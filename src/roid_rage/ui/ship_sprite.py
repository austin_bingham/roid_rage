from roid_rage.sprites import RenderedSprite
from roid_rage import util


class ShipSprite(RenderedSprite):
    def __init__(self, 
                 ship,
                 *args, **kwargs):
        super().__init__(width=ship.,
                         height=height,
                         *args, **kwargs)


    def render(self, draw):
        line_width = 4
        draw.line([(0, 0), (self.width, self.height / 2), (0, self.height),
                   (0, 0)], fill=(100, 100, 100, 255), width=line_width)