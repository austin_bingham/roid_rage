from itertools import chain
from weakref import WeakKeyDictionary

from arcade import check_for_collision_with_list, SpriteList

from dataclasses import dataclass
from roid_rage.explosion import explosion
from roid_rage.field import Field
from roid_rage.model.game import Game


@dataclass
class Model:
    game: Game
    roids: WeakKeyDictionary = WeakKeyDictionary()
    ships: WeakKeyDictionary = WeakKeyDictionary()
    explosions: SpriteList = SpriteList()
    bullets: WeakKeyDictionary = WeakKeyDictionary()

    def _all_sprites(self):
        return chain(
            self.roids.values(),
            self.ships.values(),
            self.explosions,
            self.bullets()
        )

    def draw(self):
        for s in self._all_sprites():
            s.draw()

    def update(self, delta_time):
        self.game.update(delta_time)

        for s in self._all_sprites():
            s.update()

        for elem in chain(self.roid_list, self.ship_list):
            self._wrap(elem)

        for elem in chain(self.explosion_list, self.bullet_list):
            self._out_of_bounds(elem)

        #self._collide_roids()
        #self._collide_bullets()

    def _collide_bullets(self):
        for bullet in self.bullet_list:
            hit_list = check_for_collision_with_list(bullet, self.roid_list)

            # If it did, get rid of the bullet
            if hit_list:
                bullet.kill()

            for roid in hit_list:
                roid.kill()
                for fragment in explosion(roid.center_x, roid.center_y):
                    self.explosion_list.append(fragment)

    def _collide_roids(self):
        for ship in self.ship_list:
            hit_list = check_for_collision_with_list(ship, self.roid_list)
            if hit_list:
                ship.kill()
                for fragment in explosion(ship.center_x, ship.center_y):
                    self.explosion_list.append(fragment)

    def _out_of_bounds(self, element):
        "Kill element if it's out of bounds."
        if not self.field.left_limit <= element.center_x <= self.field.right_limit:
            element.kill()
        elif not self.field.bottom_limit <= element.center_y <= self.field.top_limit:
            element.kill()

    def _wrap(self, element):
        "Wrap elements which are out of bounds."
        if element.center_x > self.field.right_limit:
            element.center_x = self.field.left_limit
        elif element.center_x < self.field.left_limit:
            element.center_x = self.field.right_limit

        if element.center_y < self.field.bottom_limit:
            element.center_y = self.field.top_limit
        elif element.center_y > self.field.top_limit:
            element.center_y = self.field.bottom_limit

