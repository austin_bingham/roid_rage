from roid_rage.sprites import RenderedSprite


class Roid(RenderedSprite):
    def __init__(self, radius, velocity, *args, **kwargs):
        self.radius = radius

        super().__init__(width=radius * 2,
                         height=radius * 2,
                         *args, **kwargs)

        self.velocity = velocity
        self.collision_radius = self.radius

    def render(self, draw):
        line_width = 3
        draw.ellipse([0, 0, self.radius * 2 - line_width / 2, self.radius * 2 - line_width / 2],
                     fill=None, outline=(100, 100, 100, 255), width=line_width)


# class Roid(drawable.Drawable, game_object.GameObject):

#     def __init__(self,
#                  size,
#                  speed,
#                  direction,
#                  position,
#                  orientation,
#                  rotation_speed,
#                  board):
#         drawable.Drawable.__init__(self, size, position, orientation)
#         game_object.GameObject.__init__(self, board)

#         self.radius = size / 3
#         self.speed = speed
#         self.rotation_speed = rotation_speed
#         self.direction = direction

#         # divide circle into 8 parts.
#         brgs = map(lambda i: math.pi / 4.0 * i, range(8))
#         self.points = [(0.5 + math.cos(brg) * util.rand(0.25, 0.5),
#                         0.5 + math.sin(brg) * util.rand(0.25, 0.5))
#                        for brg in brgs]

#     def render_image(self, ctxt):
#         ctxt.move_to(self.points[0][0],
#                      self.points[0][1])

#         for p1, p2 in zip(self.points[:-1], self.points[1:]):
#             ctxt.line_to(p2[0], p2[1])
#         ctxt.line_to(self.points[0][0], self.points[0][1])
#         ctxt.stroke()

#     def _render_image(self, ctxt):
#         'renders a simple circle.'
#         ctxt.new_sub_path()
#         ctxt.arc(0.5, 0.5, 0.5, 0.0, 2 * math.pi)
#         ctxt.stroke()

#     @util.trace
#     def update(self, time_delta):
#         dist = self.speed * time_delta

#         p = self.position + geom.Vector(dist, self.direction)
#         self.position = geom.Point(
#             p.x % self.board.width,
#             p.y % self.board.height)

#         self.orientation += self.rotation_speed

#         self.invalidate()


# def make_roid(roid_type, *args, **kwargs):
#     r = roid_type(*args, **kwargs)
#     groups.roids.add(r)
#     return r
