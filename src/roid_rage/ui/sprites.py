import PIL
import arcade


class RenderedSprite(arcade.Sprite):
    def __init__(self, width, height, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = width
        self.height = height

        image = PIL.Image.new(mode='RGBA', size=(
            width, height), color=(0, 0, 0, 0))
        draw = PIL.ImageDraw.Draw(image)
        self.render(draw)

        self.append_texture(arcade.Texture(name='ship', image=image))
        self.set_texture(0)

    def render(self, draw):
        pass
