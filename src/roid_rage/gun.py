from roid_rage import bullet


class Gun:
    def __init__(self,
                 bullet_size,
                 bullet_speed,
                 fire_rate):
        self.bullet_size = bullet_size
        self.bullet_speed = bullet_speed
        self.fire_rate = fire_rate

        self.fire_time = fire_rate

    @property
    def can_fire(self):
        'Can the gun fire at this time?'
        return self.fire_time >= self.fire_rate

    def update(self, time_delta):
        self.fire_time += time_delta

    def fire(self):
        if not self.can_fire:
            return None

        self.fire_time = 0