import io
import os
from setuptools import setup, find_packages


def local_file(*name):
    return os.path.join(
        os.path.dirname(__file__),
        *name)


def read(*names, **kwargs):
    with io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf8")
    ) as fp:
        return fp.read()


def read_version():
    """Read the `(version-string, version-info)` from
    `src/roid_rage/version.py`.
    """

    version_file = local_file(
        'src', 'roid_rage', 'version.py')
    local_vars = {}
    with open(version_file) as handle:
        exec(handle.read(), {}, local_vars)  # pylint: disable=exec-used
    return (local_vars['__version__'], local_vars['__version_info__'])


long_description = read(local_file('README.rst'), mode='rt')

setup(
    name='roid_rage',
    version=read_version()[0],
    packages=find_packages('src'),

    author='Austin Bingham',
    author_email='austin.bingham@gmail.com',
    description="Rage against the 'roids!",
    license='MIT',
    keywords='game',
    url='http://bitbucket.org/austin_bingham/roid_rage',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: X11 Applications',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Games/Entertainment :: Arcade',
    ],
    platforms='any',
    include_package_data=True,
    package_dir={'': 'src'},
    # package_data={'roid_rage': . . .},
    install_requires=[
        'baker',
        'decorator',
        'pygame',
    ],
    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax, for
    # example: $ pip install -e .[dev,test]
    extras_require={
        'dev': ['wheel'],
        # 'doc': ['sphinx', 'cartouche'],
        'test': ['pytest'],
    },
    entry_points={
        'console_scripts': [
           'roid_rage = roid_rage.game:main',
        ],
    },
    long_description=long_description,
)
