import math

import pytest
from roid_rage.collision import calculate_collision_vector
from roid_rage.geom import Point


def test_directly_right_motionless():
    a = calculate_collision_vector(
        position=Point(0, 0),
        speed=100,
        target_pos=Point(10, 0),
        target_speed=0,
        target_dir=0)
    assert a[1].bearing == pytest.approx(0.0)


def test_directly_left_motionless():
    a = calculate_collision_vector(
        position=Point(0, 0),
        speed=100,
        target_pos=Point(-10, 0),
        target_speed=0,
        target_dir=0)
    assert a[1].bearing == pytest.approx(math.pi)


def test_straight_up_motionless():
    a = calculate_collision_vector(
        position=Point(0, 0),
        speed=10,
        target_pos=Point(0, 10),
        target_speed=0,
        target_dir=0)
    assert a[1].bearing == pytest.approx(math.pi / 2.0)


def test_45_degree_angle_motionless():
    a = calculate_collision_vector(
        position=Point(0, 0),
        speed=10,
        target_pos=Point(10, 10),
        target_speed=0,
        target_dir=0)
    assert a[1].bearing == pytest.approx(math.pi / 4.0)

# TODO: Moving tests
# TODO: non-collision tests
