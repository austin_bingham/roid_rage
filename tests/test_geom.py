import math

import pytest

from roid_rage import geom


class Test_velocity_to_speed:
    def test_left(self):
        assert geom.velocity_to_speed((-2, 0)) == 2

    def test_right(self):
        assert geom.velocity_to_speed((2, 0)) == 2

    def test_up(self):
        assert geom.velocity_to_speed((0, 2)) == 2

    def test_down(self):
        assert geom.velocity_to_speed((0, -2)) == 2

    def test_angle(self):
        assert geom.velocity_to_speed((2, 2)) == pytest.approx(math.sqrt(8))


class Test_velocity_to_direction:
    def test_left(self):
        assert geom.velocity_to_direction((-2, 0)) == pytest.approx(math.pi)

    def test_right(self):
        assert geom.velocity_to_direction((2, 0)) == pytest.approx(0.0)

    def test_up(self):
        assert geom.velocity_to_direction((0, 2)) == pytest.approx(math.pi * 0.5)

    def test_down(self):
        assert geom.velocity_to_direction((0, -2)) == pytest.approx(math.pi * -0.5)